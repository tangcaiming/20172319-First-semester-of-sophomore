package week10.Chapter12_优先队列与堆.Exp2;

import week6.Chapter6_列表.jsjf.ArrayUnorderedList;
import week6.Chapter6_列表.jsjf.UnorderedListADT;
import week8.Chapter10_树.jsjf.BinaryTreeADT;
import week8.Chapter10_树.jsjf.BinaryTreeNode;
import week8.Chapter10_树.jsjf.exceptions.ElementNotFoundException;
import week8.Chapter10_树.jsjf.exceptions.EmptyCollectionException;

import java.util.*;

/**
 * LinkedBinaryTree implements the BinaryTreeADT interface
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedBinaryTree<T> implements BinaryTreeADT<T>, Iterable<T>
{
    protected BinaryTreeNode<T> root;
    protected int modCount;
    protected int count;  //  记录树中元素个数

    /**
     * Creates an empty binary tree.
     */
    public LinkedBinaryTree()
    {
        root = null;
    }

    /**
     * Creates a binary tree with the specified element as its root.
     *
     * @param element the element that will become the root of the binary tree
     */
    public LinkedBinaryTree(T element)
    {
        root = new BinaryTreeNode<T>(element);
    }

    /**
     * Creates a binary tree with the specified element as its root and the
     * given trees as its left child and right child
     *
     * @param element the element that will become the root of the binary tree
     * @param left the left subtree of this tree
     * @param right the right subtree of this tree
     */
    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left,
                            LinkedBinaryTree<T> right)
    {
        root = new BinaryTreeNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }

    /**
     * Returns a reference to the element at the root
     *
     * @return a reference to the specified target
     * @throws EmptyCollectionException if the tree is empty
     */
    @Override
    public T getRootElement() throws EmptyCollectionException
    {
        // To be completed as a Programming seatwork
        if (isEmpty()){
            throw new EmptyCollectionException("LinkedBinaryTree");
        }
        return root.element;
    }

    /**
     * Returns a reference to the node at the root
     *
     * @return a reference to the specified node
     * @throws EmptyCollectionException if the tree is empty
     */
    protected BinaryTreeNode<T> getRootNode() throws EmptyCollectionException
    {
        // To be completed as a Programming seatwork
        if (isEmpty()){
            throw new EmptyCollectionException("LinkedBinaryTree");
        }
        return root;
    }
    // 获取根结点
    public BinaryTreeNode<T> getRoot() {
        return root;
    }

    /**
     * Returns the left subtree of the root of this tree.
     *
     * @return a link to the left subtree fo the tree
     */
    public LinkedBinaryTree<T> getLeft()
    {
        // To be completed as a Programming seatwork
        LinkedBinaryTree result = new LinkedBinaryTree();
        result.root = root.getLeft();
        return result;

    }

    /**
     * Returns the right subtree of the root of this tree.
     *
     * @return a link to the right subtree of the tree
     */
    public LinkedBinaryTree<T> getRight()
    {
        // To be completed as a Programming seatwork
        LinkedBinaryTree result = new LinkedBinaryTree();
        result.root = root.getRight();
        return result;

    }

    /**
     * Returns true if this binary tree is empty and false otherwise.
     *
     * @return true if this binary tree is empty, false otherwise
     */
    @Override
    public boolean isEmpty()
    {
        if (root != null){
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Returns the integer size of this tree.
     *
     * @return the integer size of the tree
     */
    @Override
    public int size()
    {
        // To be completed as a Programming seatwork
       if (root!=null){
           return root.numChildren() + 1;
       }
       else {
           return 0;
       }
//        return count;
    }

    /**
     * Returns the height of this tree.
     *
     * @return the height of the tree
     */
     public int getHeight()
    {
        // To be completed as a Programming seatwork
//        int front = -1, rear = -1;
//        int last = 0, level = 0;
//        BinaryTreeNode[] queue = new BinaryTreeNode[100000];
//        if (root == null) {
//            return 0;
//        }
//        queue[++rear]= root;
//        BinaryTreeNode current;
//        while(front<rear){
//            current=queue[++front];
//            if(current.left!=null){
//                queue[++rear]=current.left;
//            }
//            if(current.right!=null){
//                queue[++rear]=current.right;
//            }
//            if(front==last){
//                level++;
//                last=rear;
//            }
//        }
//        return level;
        return height(root);
    }

    /**
     * Returns the height of the specified node.
     *
     * @param node the node from which to calculate the height
     * @return the height of the tree
     */
    private int height(BinaryTreeNode<T> node)
    {
        // To be completed as a Programming seatwork
        if (node==null){
            return 0;
        }
        else {
            int left = height(node.left);
            int right = height(node.right);
            return (left > right) ? (left + 1) : (right + 1);
        }
//        if (node==null){
//            return 0;
//        }
//        else {
//            int left = height(root.left);
//            int right = height(root.right);
//            if (left > right){
//                return left + 1;
//            }
//            else {
//                return right + 1;
//            }
//        }
    }

    /**
     * Returns true if this tree contains an element that matches the
     * specified target element and false otherwise.
     *
     * @param targetElement the element being sought in this tree
     * @return true if the element in is this tree, false otherwise
     */
    @Override
    public boolean contains(T targetElement)
    {
        // To be completed as a Programming seatwork
        T temp;
        boolean found = false;

        try {
            temp = find(targetElement);
            found = true;
        }
        catch (Exception ElementNotFoundExecption){
            found = false;
        }
        return found;
    }

    /**
     * Returns a reference to the specified target element if it is
     * found in this binary tree.  Throws a ElementNotFoundException if
     * the specified target element is not found in the binary tree.
     *
     * @param targetElement the element being sought in this tree
     * @return a reference to the specified target
     * @throws ElementNotFoundException if the element is not in the tree
     */
    @Override
    public T find(T targetElement) throws ElementNotFoundException
    {
        BinaryTreeNode<T> current = findNode(targetElement, root);

        if (current == null) {
            throw new ElementNotFoundException("LinkedBinaryTree");
        }

        return (current.getElement());
    }

    /**
     * Returns a reference to the specified target element if it is
     * found in this binary tree.
     *
     * @param targetElement the element being sought in this tree
     * @param next the element to begin searching from
     */
    private BinaryTreeNode<T> findNode(T targetElement,
                                        BinaryTreeNode<T> next)
    {
        if (next == null) {
            return null;
        }

        if (next.getElement().equals(targetElement)) {
            return next;
        }

        BinaryTreeNode<T> temp = findNode(targetElement, next.getLeft());

        if (temp == null) {
            temp = findNode(targetElement, next.getRight());
        }

        return temp;
    }




    // 获取某一结点的左子树
    public  String getNodeLeftTree(T Elemnet){
        String result;
        BinaryTreeNode node = new BinaryTreeNode(Elemnet);
        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
        linkedBinaryTree.root = root;
        if (root==null){
            return "";
        }
        else {
            if (root != null && root.left == null && root.right == null) {
                linkedBinaryTree.root = root;
                result = linkedBinaryTree.printTree();
                return result;
            }
            node = findNode(Elemnet,root);
            if (node.left!=null){
                linkedBinaryTree.root = node.left;
                result = linkedBinaryTree.printTree();
            }
            else {
                result = "该结点无左子树";
            }
            return result;
        }
    }
    // 获取某一结点的右子树
    public  String getNodeRightTree(T Elemnet){
        String result;
        BinaryTreeNode node = new BinaryTreeNode(Elemnet);
        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
        linkedBinaryTree.root = root;
        if (root==null){
            return "";
        }
        else {
            if (root != null && root.left == null && root.right == null) {
                linkedBinaryTree.root = root;
                result = linkedBinaryTree.printTree();
                return result;
            }
            node = findNode(Elemnet,root);
            if (node.right!=null){
                linkedBinaryTree.root = node.right;
                result = linkedBinaryTree.printTree();
            }
            else {
                result = "该结点无右子树";
            }
            return result;
        }
    }
    //  删除左子树
    public LinkedBinaryTree removeLeftsubtree(){
        LinkedBinaryTree Leftsubtree = new LinkedBinaryTree();
        Leftsubtree.root = root;
        root.left = null;
        return Leftsubtree;
    }
    //  删除右子树
    public LinkedBinaryTree removeRightsubtree(){
        LinkedBinaryTree Rightsubtree = new LinkedBinaryTree();
        Rightsubtree.root = root;
        root.right = null;
        return Rightsubtree;
    }
    //     删除指定结点的左子树
    public LinkedBinaryTree removeNodeLeftsubtree(T Element){
        BinaryTreeNode node = new BinaryTreeNode(Element);
        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
        linkedBinaryTree.root = root;
        if (root==null){
            return linkedBinaryTree;
        }
        else {
            if (root != null && root.left == null && root.right == null) {
                linkedBinaryTree.root = root;
                return linkedBinaryTree;
            }
            node = findNode(Element,root);
            node.left = null;
            linkedBinaryTree.root = root;
            return linkedBinaryTree;
        }
    }
    //     删除指定结点的右子树
    public LinkedBinaryTree removeNodeRightsubtree(T Element){
        BinaryTreeNode node = new BinaryTreeNode(Element);
        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
        linkedBinaryTree.root = root;
        if (root==null){
            return linkedBinaryTree;
        }
        else {
            if (root != null && root.left == null && root.right == null) {
                linkedBinaryTree.root = root;
                return linkedBinaryTree;
            }
            node = findNode(Element,root);
            node.right = null;
            linkedBinaryTree.root = root;
            return linkedBinaryTree;
        }
    }
    // 删除所有元素
    public LinkedBinaryTree removeAllElements(){
        LinkedBinaryTree Tree = new LinkedBinaryTree();
        Tree.root = null;
        return Tree;
    }
    // 插入元素
    public void add(T Element){
        BinaryTreeNode node = new BinaryTreeNode(Element);
        if (root == null) {
            root = node;
            count++;
            root.left = null;
            root.right = null;
        } else {
            BinaryTreeNode current = root;
            BinaryTreeNode parent = null;
            while (true) {
                if ((int)Element <  (int)current.element) {
                    parent = current;
                    current = current.left;
                    if (current == null) {
                        parent.left = node;
                        count++;
                        break;
                    }
                } else if ((int)Element > (int)current.element) {
                    parent = current;
                    current = current.right;
                    if (current == null) {
                        parent.right = node;
                        count++;
                        break;
                    }
                }
            }
        }
    }

    // 判断某一结点是叶子结点还是内部结点
    protected String Judging_results;
    public boolean Judgenode(T Element) {
        BinaryTreeNode<T> node = new BinaryTreeNode<T>(Element);
        if (root == null){
            Judging_results = "It's not a leaf or an internal node .";
            return false;
        }
        else {
            if (root != null&&root.left==null&&root.right==null){
                Judging_results = "Both leaves and internal nodes .";
            }
            node = findNode(Element,root);
            if (node.left!=null||node.right!=null){
                Judging_results = "internal nodes .";
                return true;
            }
            else {
                Judging_results = "leaf nodes . ";
                return false;
            }
        }
    }







    //  计算树的叶子结点个数
    public int CountLeaf(BinaryTreeNode T , int countLeaf){
        if (T == null){
            countLeaf = 0 ;
        }
        if (T.left==null&&T.left==null){
            System.out.println("叶子结点 ： " + T.getElement());
            countLeaf = 1;
        }
        else {
            return CountLeaf(T.getLeft(),countLeaf) + CountLeaf(T.getRight(),countLeaf);
        }
        return countLeaf;
    }

    // 递归实现层级遍历
    public void LevelOrder_recursion(BinaryTreeNode<T> node){
        if (node==null){
            System.out.println("This node is null");
        }
        int height = height(root);
        for (int i = 1; i <= height ; i++){
            Level_traversal(node,i);
        }
    }
    int num = 0;
    protected void Level_traversal(BinaryTreeNode<T> node ,int Level ){
        if (node == null || Level<1){
            return;
        }
        else {
            if (Level==1){
                num++;
                System.out.println("第" + num + "个结点 ： " + node.getElement());
                return;
            }
        }

        Level_traversal(node.left,Level-1);
        Level_traversal(node.right,Level-1);
    }
    // 非递归实现层级遍历
    public void LevelOrder_norecursion(){
        ArrayUnorderedList<BinaryTreeNode<T>> templist = new ArrayUnorderedList<BinaryTreeNode<T>>();
        BinaryTreeNode current = root;
        templist.addToFront(root);
        num = 0;
        while (!templist.isEmpty()){
            num++;
            current = templist.first();
            System.out.println("第" + num + "个结点 ： " + templist.first().getElement());
            if (current.getLeft()!=null||current.getRight()!=null){
                if (current.getLeft()!=null){
                    templist.addToRear(current.getLeft());
                }
                if (current.getRight()!=null){
                    templist.addToRear(current.getRight());
                }

            }
            templist.removeFirst();
        }
    }

    /**
     * 基于LinkedBinaryTree，实现基于（中序，先序）序列构造唯一一棵二㕚树的功能，比如给出中序HDIBEMJNAFCKGL和后序ABDHIEJMNCFGKL,构造出附图中的树

     用JUnit或自己编写驱动类对自己实现的功能进行测试，提交测试代码运行截图，要全屏，包含自己的学号信息

     课下把代码推送到代码托管平台
     * @param preorder
     * @param inorder
     * @return
     */

    //  前序中序构建二叉树
    public BinaryTreeNode BuildTree(char[] preorder, char[] inorder) {
        return BuildLinkedBinaryTree(preorder, inorder, 0, inorder.length - 1, inorder.length);
    }

    /**
     * @param preorder 前序
     * @param inorder  中序
     * @param Start 起始位置
     * @param End 终止位置
     * @param length 结点个数
     */
    public BinaryTreeNode BuildLinkedBinaryTree(char[] preorder,char[] inorder,int Start, int End,int length) {
        if (preorder==null||preorder.length == 0 || inorder == null
                || inorder.length == 0 || length <= 0){
            return null;
        }
        BinaryTreeNode binaryTreeNode;
        binaryTreeNode = new BinaryTreeNode(preorder[Start]);
        if (length==1){
            return binaryTreeNode;
        }
        int flag=0;
        while (flag < length){
            if (preorder[Start] == inorder[End - flag]){
                break;
            }
            flag++;
        }
        binaryTreeNode.left = BuildLinkedBinaryTree(preorder, inorder,Start + 1,  End - flag - 1, length - 1 - flag);
        binaryTreeNode.right = BuildLinkedBinaryTree(preorder, inorder,Start + length - flag,  End, flag );
        return binaryTreeNode;
    }

    /**
     * Returns a string representation of this binary tree showing
     * the nodes in an inorder fashion.
     *
     * @return a string representation of this binary tree
     */
    @Override
    public String toString()
    {
        // To be completed as a Programming seatwork
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preOrder(root,tempList);

        return tempList.toString();
    }

    public String printTree()
    {
        UnorderedListADT<BinaryTreeNode<T>> nodes =
                new ArrayUnorderedList<BinaryTreeNode<T>>();
        UnorderedListADT<Integer> levelList =
                new ArrayUnorderedList<Integer>();
        BinaryTreeNode<T> current;
        String result = "";
        int printDepth = this.getHeight();
        int possibleNodes = (int)Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes)
        {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel)
            {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++) {
                    result = result + " ";
                }
            }
            else
            {
                for (int i = 0; i < ((Math.pow(2, (printDepth - currentLevel + 1)) - 1)) ; i++)
                {
                    result = result + " ";
                }
            }
            if (current != null)
            {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            }
            else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }

        }

        return result;
    }


    /**
     * Returns an iterator over the elements in this tree using the
     * iteratorInOrder method
     *
     * @return an in order iterator over this binary tree
     */
    @Override
    public Iterator<T> iterator()
    {
        return iteratorInOrder();
    }

    /**
     * Performs an inorder traversal on this binary tree by calling an
     * overloaded, recursive inorder method that starts with
     * the root.
     *
     * @return an in order iterator over this binary tree
     */
    @Override
    public Iterator<T> iteratorInOrder()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        inOrder(root, tempList);

        return new TreeIterator(tempList.iterator());
    }

    /**
     * Performs a recursive inorder traversal.
     *
     * @param node the node to be used as the root for this traversal
     * @param tempList the temporary list for use in this traversal
     */
    private void inOrder(BinaryTreeNode<T> node,
                           ArrayUnorderedList<T> tempList)
    {
        if (node != null)
        {
            inOrder(node.getLeft(), tempList);
            tempList.addToRear(node.getElement());
            inOrder(node.getRight(), tempList);
        }
    }

    /**
     * Performs an preorder traversal on this binary tree by calling
     * an overloaded, recursive preorder method that starts with
     * the root.
     *
     * @return a pre order iterator over this tree
     */
    @Override
    public Iterator<T> iteratorPreOrder()
    {
        // To be completed as a Programming seatwork
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preOrder (root, tempList);

        return tempList.iterator();

    }

    /**
     * Performs a recursive preorder traversal.
     *
     * @param node the node to be used as the root for this traversal
     * @param tempList the temporary list for use in this traversal
     */
    private void preOrder(BinaryTreeNode<T> node,
                            ArrayUnorderedList<T> tempList)
    {
        // To be completed as a Programming seatwork
        if (node!=null){
            tempList.addToRear(node.element);
            preOrder(node.left,tempList);
            preOrder(node.right,tempList);
        }

    }

    /**
     * Performs an postorder traversal on this binary tree by calling
     * an overloaded, recursive postorder method that starts
     * with the root.
     *
     * @return a post order iterator over this tree
     */
    @Override
    public Iterator<T> iteratorPostOrder()
    {
        // To be completed as a Programming seatwork
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        postOrder (root, tempList);

        return tempList.iterator();
    }

    /**
     * Performs a recursive postorder traversal.
     *
     * @param node the node to be used as the root for this traversal
     * @param tempList the temporary list for use in this traversal
     */
    private void postOrder(BinaryTreeNode<T> node,
                             ArrayUnorderedList<T> tempList)
    {
        // To be completed as a Programming seatwork
        if (node != null)
        {
            postOrder(node.getLeft(), tempList);
            postOrder(node.getRight(), tempList);
            tempList.addToRear(node.getElement());
        }
    }

    /**
     * Performs a levelorder traversal on this binary tree, using a
     * templist.
     *
     * @return a levelorder iterator over this binary tree
     */
    @Override
    public Iterator<T> iteratorLevelOrder()
    {
        ArrayUnorderedList<BinaryTreeNode<T>> nodes =
                              new ArrayUnorderedList<BinaryTreeNode<T>>();
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        BinaryTreeNode<T> current;

        nodes.addToRear(root);

        while (!nodes.isEmpty())
        {
            current = nodes.removeFirst();

            if (current != null)
            {
                tempList.addToRear(current.getElement());
                if (current.getLeft() != null) {
                    nodes.addToRear(current.getLeft());
                }
                if (current.getRight() != null) {
                    nodes.addToRear(current.getRight());
                }
            }
            else {
                tempList.addToRear(null);
            }
        }

        return new TreeIterator(tempList.iterator());
    }

    /**
     * Inner class to represent an iterator over the elements of this tree
     */
    private class TreeIterator implements Iterator<T>
    {
        private int expectedModCount;
        private Iterator<T> iter;

        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a tree traversal
         */
        public TreeIterator(Iterator<T> iter)
        {
            this.iter = iter;
            expectedModCount = modCount;
        }

        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return  true if this iterator has at least one more element to deliver
         *          in the iteration
         * @throws  ConcurrentModificationException if the collection has changed
         *          while the iterator is in use
         */
        @Override
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (!(modCount == expectedModCount)) {
                throw new ConcurrentModificationException();
            }

            return (iter.hasNext());
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        @Override
        public T next() throws NoSuchElementException
        {
            if (hasNext()) {
                return (iter.next());
            } else {
                throw new NoSuchElementException();
            }
        }

        /**
         * The remove operation is not supported.
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        @Override
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
        }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
        System.out.println("实验二-1-实现二叉树:");
        linkedBinaryTree.add(50);
        linkedBinaryTree.add(30);
        linkedBinaryTree.add(40);
        linkedBinaryTree.add(20);
        linkedBinaryTree.add(32);
        linkedBinaryTree.add(35);
        linkedBinaryTree.add(80);
        linkedBinaryTree.add(90);
        linkedBinaryTree.add(85);
        linkedBinaryTree.add(88);
        System.out.println("打印树：");
        System.out.println(linkedBinaryTree.printTree());
        System.out.print("测试getRight；");
        System.out.println("输入所需获取右子树的结点存储值:");
        int input = scanner.nextInt();
        System.out.println(linkedBinaryTree.getNodeRightTree(input));
        System.out.print("测试contains；");
        System.out.println("输入你想判断的元素值：");
        input = scanner.nextInt();
        System.out.println(linkedBinaryTree.contains(input));
        System.out.print("测试toString:   ");
        System.out.println(linkedBinaryTree.toString());
        System.out.print("测试preorder:   ");
        Iterator iterator = linkedBinaryTree.iteratorPreOrder();
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        System.out.print("测试inorder：   ");
        iterator = linkedBinaryTree.iteratorInOrder();
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
        System.out.print("测试postorder:   ");
        iterator = linkedBinaryTree.iteratorPostOrder();
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }
        linkedBinaryTree.removeAllElements();
        System.out.println();

        System.out.println("实验二 树-2-中序先序序列构造二叉树:");
        String preorderinput,inorderinput;

        System.out.println("先序序列为："); // A B D H I E J M N C F G K L
        preorderinput = "A B D H I E J M N C F G K L";
        System.out.println(preorderinput);
        System.out.println("中序序列为"); //  H D I B E M J N A F C K G L
        inorderinput = "H D I B E M J N A F C K G L";
        System.out.println(inorderinput);
        char[] preorder = new char[]{'A','B','D' ,'H' ,'I' ,'E' ,'J' ,'M', 'N' ,'C' ,'F', 'G' ,'K', 'L'};
        char[] inorder = new char[]{'H' ,'D', 'I', 'B', 'E', 'M', 'J', 'N', 'A', 'F', 'C', 'K', 'G', 'L'};
        System.out.println("构造的二叉树为：");
        linkedBinaryTree.root = linkedBinaryTree.BuildTree(preorder,inorder);
        System.out.println(linkedBinaryTree.printTree());
    }
}
