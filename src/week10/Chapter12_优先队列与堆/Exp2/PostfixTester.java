package week10.Chapter12_优先队列与堆.Exp2;


import java.util.Iterator;
import java.util.Scanner;


/**
 * Demonstrates the use of an expression tree to evaluate postfix expressions.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class PostfixTester    
{
    /**
     * Reads and evaluates multiple postfix expressions.
     */
    public static void main(String[] args)
    {
        String expression, again;
        int result;
    
        Scanner in = new Scanner(System.in);
      
//        do
//        {
            PostfixEvaluator evaluator = new PostfixEvaluator();
            System.out.println("请输入一个中序表达式：");
//            System.out.println("Enter a valid post-fix expression one token " +
//                               "at a time with a space between each token (e.g. 5 4 + 3 2 1 - + *)");
//            System.out.println("Each token must be an integer or an operator (+,-,*,/)");
            expression = in.nextLine() ; // 4 + 3 * 6 / 2 - 5 * 3
            String[] temp_expression = expression.split(" ");
            LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
            linkedBinaryTree.root = evaluator.Build_Expression_Tree(temp_expression,temp_expression.length);
            expression = "";
            System.out.println("由中序表达式构建的树为：");
            System.out.println(linkedBinaryTree.printTree());
            System.out.println("后序遍历获取树的后缀表达式：");
            Iterator iterator = linkedBinaryTree.iteratorPostOrder();
            while (iterator.hasNext()){
                expression += iterator.next() + " ";
            }
            System.out.println(expression);
            System.out.println("输出后缀表达式树的结果：");
            System.out.println(evaluator.evaluate(expression));
//            System.out.println(evaluator.getTree());
//            System.out.println(evaluator.getTree());
//            Iterator iterator = evaluator.iteratorPostOrder();
//            expression = "";
//            while (iterator!=null){
//                expression += iterator.next() + " ";
//            }
//            result = evaluator.evaluate(expression);
//            System.out.println();
//            System.out.println("That expression equals " + result);
//
//            System.out.println("The Expression Tree for that expression is: ");
//            System.out.println(evaluator.getTree());
////
//            System.out.print("Evaluate another expression [Y/N]? ");
//            again = in.nextLine();
//            System.out.println();
////        }
//        while (again.equalsIgnoreCase("y"));
   }
}
