package week10.Chapter12_优先队列与堆.jsjf.seatwork;

import week10.Chapter12_优先队列与堆.jsjf.ArrayHeap;

/**
 * HeapSort sorts a given array of Comparable objects using a heap.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class HeapSort<T>
{
    /**
     * Sorts the specified array using a Heap
	 *
	 * @param data the data to be added to the heapsort
     */
	public void HeapSort(T[] data)
	{
		ArrayHeap<T> temp = new ArrayHeap<T>();

		// copy the array into a heap
		for (T aData : data) {
			temp.addElement(aData);
		}

		// place the sorted elements back into the array
		int count = 0;
		while (!(temp.isEmpty()))
		{
			data[count] = temp.removeMax();
			count++;
		}
	}
}


