package week10.Chapter12_优先队列与堆.jsjf.seatwork;

import week10.Chapter12_优先队列与堆.jsjf.seatwork.ArrayHeap;
import week10.Chapter12_优先队列与堆.jsjf.seatwork.ArrayBinaryTree;

import java.util.Iterator;

public class 堆排序测试 {
    public static void main(String[] args) {

        ArrayHeap arrayHeap = new ArrayHeap();
        arrayHeap.addElement(36);
        arrayHeap.addElement(30);
        arrayHeap.addElement(18);
        arrayHeap.addElement(40);
        arrayHeap.addElement(32);
        arrayHeap.addElement(45);
        arrayHeap.addElement(22);
        arrayHeap.addElement(50);
        System.out.println("构建好的大顶堆经过层序遍历为：   ");
        Iterator iterator = arrayHeap.iteratorLevelOrder();
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        int i = 0;
        String result="",Orderly_array = "";
        while (!arrayHeap.isEmpty()){
            Orderly_array +=arrayHeap.removeMax() + " ";
            result = "";
            iterator = arrayHeap.iteratorLevelOrder();
            while (iterator.hasNext()){
                result += iterator.next() + " ";
            }
            System.out.print("经历一轮排序后的大顶堆层级遍历为：");
            System.out.println(result);
            System.out.print("经历一轮排序后的有序数组为：");
            System.out.println(Orderly_array);
            i++;
        }
    }
}
