package week10.Chapter12_优先队列与堆.PP项目;

import week10.Chapter12_优先队列与堆.PP项目.PrioritizedObject;
import week10.Chapter12_优先队列与堆.jsjf.ArrayHeap;

/**
 * PriorityQueue implements a priority queue using a heap.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class PriorityQueue<T> extends ArrayHeap<PrioritizedObject<T>>
{
    /**
     * Creates an empty priority queue.
     */
    public PriorityQueue()
    {
        super();
    }

    /**
     * Adds the given element to this PriorityQueue.
     *
     * @param object the element to be added to the priority queue
     * @param priority the integer priority of the element to be added
     */
    public void addElement(T object, int priority)
    {
        PrioritizedObject<T> obj = new PrioritizedObject<T>(object, priority);
	    super.addElement(obj);
    }

    /**
     * Removes the next highest priority element from this priority
     * queue and returns a reference to it.
     *
     * @return a reference to the next highest priority element in this queue
     */
    public T removeNext()
    {
        PrioritizedObject<T> obj = (PrioritizedObject<T>)super.removeMin();
        return obj.getElement();
    }

    public static void main(String[] args) {
        PriorityQueue priorityQueue = new PriorityQueue();
        priorityQueue.addElement("20172301郭恺",1);
        priorityQueue.addElement("20172305谭鑫",5);
        priorityQueue.addElement("20172304段志轩",4);
        priorityQueue.addElement("20172302侯泽洋",2);
        priorityQueue.addElement("20172303范雯琪",3);

        System.out.println(priorityQueue);


    }
}


