package week8.Chapter10_树.jsjf;

import java.util.*;
import week6.Chapter6_列表.jsjf.ArrayUnorderedList;
import week8.Chapter10_树.jsjf.exceptions.ElementNotFoundException;
import week8.Chapter10_树.jsjf.exceptions.EmptyCollectionException;

/**
 * LinkedBinaryTree implements the BinaryTreeADT interface
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedBinaryTree<T> implements BinaryTreeADT<T>, Iterable<T>
{
    protected BinaryTreeNode<T> root; 
    protected int modCount;
    protected int count;  //  记录树中元素个数
    
    /**
     * Creates an empty binary tree.
     */
    public LinkedBinaryTree() 
    {
        root = null;
    }

    /**
     * Creates a binary tree with the specified element as its root.
     *
     * @param element the element that will become the root of the binary tree
     */
    public LinkedBinaryTree(T element) 
    {
        root = new BinaryTreeNode<T>(element);
    }
    
    /**
     * Creates a binary tree with the specified element as its root and the 
     * given trees as its left child and right child
     *
     * @param element the element that will become the root of the binary tree
     * @param left the left subtree of this tree
     * @param right the right subtree of this tree
     */
    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left, 
                            LinkedBinaryTree<T> right) 
    {
        root = new BinaryTreeNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }
    
    /**
     * Returns a reference to the element at the root
     *
     * @return a reference to the specified target
     * @throws EmptyCollectionException if the tree is empty
     */
    @Override
    public T getRootElement() throws EmptyCollectionException
    {
        // To be completed as a Programming Project
        if (isEmpty()){
            throw new EmptyCollectionException("LinkedBinaryTree");
        }
        return root.element;
    }
    
    /**
     * Returns a reference to the node at the root
     *
     * @return a reference to the specified node
     * @throws EmptyCollectionException if the tree is empty
     */
    protected BinaryTreeNode<T> getRootNode() throws EmptyCollectionException
    {
        // To be completed as a Programming Project
        if (isEmpty()){
            throw new EmptyCollectionException("LinkedBinaryTree");
        }
        return root;
    }
    
    /**
     * Returns the left subtree of the root of this tree.
     *
     * @return a link to the left subtree fo the tree
     */
    public LinkedBinaryTree<T> getLeft()
    {
        // To be completed as a Programming Project
        return new LinkedBinaryTree(root.getLeft());

    }
    
    /**
     * Returns the right subtree of the root of this tree.
     *
     * @return a link to the right subtree of the tree
     */
    public LinkedBinaryTree<T> getRight()
    {
        // To be completed as a Programming Project
        return new LinkedBinaryTree(root.getRight());
    }
    
    /**
     * Returns true if this binary tree is empty and false otherwise.
     *
     * @return true if this binary tree is empty, false otherwise
     */
    @Override
    public boolean isEmpty()
    {
        return (root == null);
    }

    /**
     * Returns the integer size of this tree.
     *
     * @return the integer size of the tree
     */
    @Override
    public int size()
    {
        // To be completed as a Programming Project
//       if (root!=null){
//           return root.numChildren() + 1;
//       }
//       else {
//           return 0;
//       }
        return count;
    }
    
    /**
     * Returns the height of this tree.
     *
     * @return the height of the tree
     */
     public int getHeight()
    {
        // To be completed as a Programming Project
        int front = -1, rear = -1;
        int last = 0, level = 0;
        BinaryTreeNode[] queue = new BinaryTreeNode[100000];
        if (root == null) {
            return 0;
        }
        queue[++rear]= root;
        BinaryTreeNode current;
        while(front<rear){
            current=queue[++front];
            if(current.left!=null){
                queue[++rear]=current.left;
            }
            if(current.right!=null){
                queue[++rear]=current.right;
            }
            if(front==last){
                level++;
                last=rear;
            }
        }
        return level;
    }
    
    /**
     * Returns the height of the specified node.
     *
     * @param node the node from which to calculate the height
     * @return the height of the tree
     */
    private int height(BinaryTreeNode<T> node) 
    {
        // To be completed as a Programming Project
        if (node.left==null&&node.right==null){
            return 0;
        }
        else {
            int left = height(node.left);
            int right = height(node.right);
            if (left > right){
                return left + 1;
            }
            else {
                return right + 1;
            }
        }
//        if (node==null){
//            return 0;
//        }
//        else {
//            int left = height(root.left);
//            int right = height(root.right);
//            if (left > right){
//                return left + 1;
//            }
//            else {
//                return right + 1;
//            }
//        }
    }
    
    /**
     * Returns true if this tree contains an element that matches the
     * specified target element and false otherwise.
     *
     * @param targetElement the element being sought in this tree
     * @return true if the element in is this tree, false otherwise
     */
    @Override
    public boolean contains(T targetElement)
    {
        // To be completed as a Programming Project
        T temp;
        boolean found = false;

        try {
            temp = find(targetElement);
            found = true;
        }
        catch (Exception ElementNotFoundExecption){
            found = false;
        }
        return found;
    }
    
    /**
     * Returns a reference to the specified target element if it is
     * found in this binary tree.  Throws a ElementNotFoundException if
     * the specified target element is not found in the binary tree.
     *
     * @param targetElement the element being sought in this tree
     * @return a reference to the specified target
     * @throws ElementNotFoundException if the element is not in the tree
     */
    @Override
    public T find(T targetElement) throws ElementNotFoundException
    {
        BinaryTreeNode<T> current = findNode(targetElement, root);
        
        if (current == null) {
            throw new ElementNotFoundException("LinkedBinaryTree");
        }
        
        return (current.getElement());
    }

    /**
     * Returns a reference to the specified target element if it is
     * found in this binary tree.
     *
     * @param targetElement the element being sought in this tree
     * @param next the element to begin searching from
     */
    private BinaryTreeNode<T> findNode(T targetElement, 
                                        BinaryTreeNode<T> next)
    {
        if (next == null) {
            return null;
        }
        
        if (next.getElement().equals(targetElement)) {
            return next;
        }
        
        BinaryTreeNode<T> temp = findNode(targetElement, next.getLeft());
        
        if (temp == null) {
            temp = findNode(targetElement, next.getRight());
        }
        
        return temp;
    }
    
    /**
     * Returns a string representation of this binary tree showing
     * the nodes in an inorder fashion.
     *
     * @return a string representation of this binary tree
     */
    @Override
    public String toString()
    {
        // To be completed as a Programming Project
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preOrder(root,tempList);

        return tempList.toString();
    }
    public void showTree(){
        Stack globalStack=new Stack();
        globalStack.push(root);
        // int nBlank=32;
        int nBlank=64;
        boolean isRowEmpty=false;
        String dot="............................";
        System.out.println(dot+dot+dot);
        while (isRowEmpty==false){
            Stack localStack=new Stack();
            isRowEmpty=true;
            for (int j=0;j<nBlank;j++)//{
            {

                System.out.print("-");
            }
            while (globalStack.isEmpty()==false){
                //里面的while循环用于查看全局的栈是否为空
                BinaryTreeNode temp=(BinaryTreeNode) globalStack.pop();
                if (temp!=null){
                    System.out.print(temp.element);
                    localStack.push(temp.left);
                    localStack.push(temp.right);
                    //如果当前的节点下面还有子节点，则必须要进行下一层的循环
                    if (temp.left!=null||temp.right!=null){
                        isRowEmpty=false;
                    }
                }else {
                    //如果全局的栈则不为空
                    System.out.print("#!");
                    localStack.push(null);
                    localStack.push(null);

                }
                //打印一些空格
                for (int j=0;j<nBlank*2-2;j++){
                    //System.out.print("&");
                    System.out.print("-");
                }
            }//while end
            System.out.println();
            nBlank/=2;
            //这个while循环用来判断，local栈是否为空,不为空的话，则取出来放入全局栈中
            while (localStack.isEmpty()==false){
                globalStack.push(localStack.pop());
            }
        }//大while循环结束之后，输出换行
        System.out.println(dot+dot+dot);
    }

    /**
     * Returns an iterator over the elements in this tree using the 
     * iteratorInOrder method
     *
     * @return an in order iterator over this binary tree
     */
    @Override
    public Iterator<T> iterator()
    {
        return iteratorInOrder();
    }
    
    /**
     * Performs an inorder traversal on this binary tree by calling an
     * overloaded, recursive inorder method that starts with
     * the root.
     *
     * @return an in order iterator over this binary tree
     */
    @Override
    public Iterator<T> iteratorInOrder()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        inOrder(root, tempList);
        
        return new TreeIterator(tempList.iterator());
    }

    /**
     * Performs a recursive inorder traversal.
     *
     * @param node the node to be used as the root for this traversal
     * @param tempList the temporary list for use in this traversal
     */
    protected void inOrder(BinaryTreeNode<T> node, 
                           ArrayUnorderedList<T> tempList) 
    {
        if (node != null)
        {
            inOrder(node.getLeft(), tempList);
            tempList.addToRear(node.getElement());
            inOrder(node.getRight(), tempList);
        }
    }

    /**
     * Performs an preorder traversal on this binary tree by calling
     * an overloaded, recursive preorder method that starts with
     * the root.
     *
     * @return a pre order iterator over this tree
     */
    @Override
    public Iterator<T> iteratorPreOrder()
    {
        // To be completed as a Programming Project
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preOrder (root, tempList);

        return tempList.iterator();

    }

    /**
     * Performs a recursive preorder traversal.
     *
     * @param node the node to be used as the root for this traversal
     * @param tempList the temporary list for use in this traversal
     */
    protected void preOrder(BinaryTreeNode<T> node,
                            ArrayUnorderedList<T> tempList)
    {
        // To be completed as a Programming Project
        if (node!=null){
            tempList.addToRear(node.element);
            preOrder(node.left,tempList);
            preOrder(node.right,tempList);
        }

    }

    /**
     * Performs an postorder traversal on this binary tree by calling
     * an overloaded, recursive postorder method that starts
     * with the root.
     *
     * @return a post order iterator over this tree
     */
    @Override
    public Iterator<T> iteratorPostOrder()
    {
        // To be completed as a Programming Project
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        postOrder (root, tempList);

        return tempList.iterator();
    }

    /**
     * Performs a recursive postorder traversal.
     *
     * @param node the node to be used as the root for this traversal
     * @param tempList the temporary list for use in this traversal
     */
    protected void postOrder(BinaryTreeNode<T> node,
                             ArrayUnorderedList<T> tempList)
    {
        // To be completed as a Programming Project
        if (node != null)
        {
            postOrder(node.getLeft(), tempList);
            postOrder(node.getRight(), tempList);
            tempList.addToRear(node.getElement());
        }
    }

    /**
     * Performs a levelorder traversal on this binary tree, using a
     * templist.
     *
     * @return a levelorder iterator over this binary tree
     */
    @Override
    public Iterator<T> iteratorLevelOrder()
    {
        ArrayUnorderedList<BinaryTreeNode<T>> nodes = 
                              new ArrayUnorderedList<BinaryTreeNode<T>>();
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        BinaryTreeNode<T> current;

        nodes.addToRear(root);
        
        while (!nodes.isEmpty()) 
        {
            current = nodes.removeFirst();
            
            if (current != null)
            {
                tempList.addToRear(current.getElement());
                if (current.getLeft() != null) {
                    nodes.addToRear(current.getLeft());
                }
                if (current.getRight() != null) {
                    nodes.addToRear(current.getRight());
                }
            }
            else {
                tempList.addToRear(null);
            }
        }
        
        return new TreeIterator(tempList.iterator());
    }
    
    /**
     * Inner class to represent an iterator over the elements of this tree
     */
    private class TreeIterator implements Iterator<T>
    {
        private int expectedModCount;
        private Iterator<T> iter;
        
        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a tree traversal
         */
        public TreeIterator(Iterator<T> iter)
        {
            this.iter = iter;
            expectedModCount = modCount;
        }
        
        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return  true if this iterator has at least one more element to deliver
         *          in the iteration
         * @throws  ConcurrentModificationException if the collection has changed
         *          while the iterator is in use
         */
        @Override
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (!(modCount == expectedModCount)) {
                throw new ConcurrentModificationException();
            }
            
            return (iter.hasNext());
        }
        
        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        @Override
        public T next() throws NoSuchElementException
        {
            if (hasNext()) {
                return (iter.next());
            } else {
                throw new NoSuchElementException();
            }
        }
        
        /**
         * The remove operation is not supported.
         * 
         * @throws UnsupportedOperationException if the remove operation is called
         */
        @Override
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
        }
    //  删除左子树
    public LinkedBinaryTree removeLeftsubtree(){
       LinkedBinaryTree Leftsubtree = new LinkedBinaryTree();
       Leftsubtree.root = root;
       root.left = null;
       return Leftsubtree;
    }
    //  删除右子树
    public LinkedBinaryTree removeRightsubtree(){
        LinkedBinaryTree Rightsubtree = new LinkedBinaryTree();
        Rightsubtree.root = root;
        root.right = null;
        return Rightsubtree;
    }
//     删除指定结点的左子树
    public LinkedBinaryTree removeNodeLeftsubtree(T Element){
        BinaryTreeNode node = new BinaryTreeNode(Element);
        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
        linkedBinaryTree.root = root;
        if (root==null){
            return linkedBinaryTree;
        }
        else {
            if (root != null && root.left == null && root.right == null) {
                linkedBinaryTree.root = root;
                return linkedBinaryTree;
            }
            node = findNode(Element,root);
            node.left = null;
            linkedBinaryTree.root = root;
            return linkedBinaryTree;

        }

//            //递归删除二叉树中以x为根的子树，（flag为标志）
////        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
////        linkedBinaryTree.root = root;
//            if(current == null)
//            {
//                return 0 ;
//            }
//            else
//            {
//
//                if(current.element == Element)    //如果当前节点的值为x，则更改标志位，在下面将向递归子函数中传递flag值
//                {
//                    flag = 1;
//                }
//                int lefttree = removeNodeLeftsubtree(current, Element,flag);  //递归左子树，lef_ret为从左子树中返回的信息
//                int righttree = removeNodeLeftsubtree(current,Element,flag);  //递归右子树，rig_ret为从右子树中返回的信息
//                if(1 == flag)       //如果标志为1，说明其祖父结点中有x，也就是说当前结点需要删除
//                {
//                    if(current.element == Element)    //如果是x结点，则需要向上层结点传递信息，以便其父节点将对应的指针域赋空
//                    {
//                        return 1;
//                    }
//                    current = null;
//                }
//                else
//                {
//                    if(1 == lefttree)    //从子结点接受收的信息，即如果其子结点为x，需要将其指针域赋空
//                    {
//                       current.left = null;
//                    }
//                    if(1 == righttree )   //从子结点接受收的信息，即如果其子结点为x，需要将其指针域赋空
//                    {
//                        current.right = null;
//                    }
//                }
//            }
//            return  0;
    }
    // 删除所有元素
    public LinkedBinaryTree removeAllElements(){
        LinkedBinaryTree Tree = new LinkedBinaryTree();
        Tree.root = null;
        return Tree;
    }
    // 插入元素
    public void add(T Element){
//                String error = null;
        BinaryTreeNode node = new BinaryTreeNode(Element);
        if (root == null) {
            root = node;
            count++;
            root.left = null;
            root.right = null;
        } else {
            BinaryTreeNode current = root;
            BinaryTreeNode parent = null;
            while (true) {
                if ((int)Element <  (int)current.element) {
                    parent = current;
                    current = current.left;
                    if (current == null) {
                        parent.left = node;
                        count++;
                        break;
                    }
                } else if ((int)Element > (int)current.element) {
                    parent = current;
                    current = current.right;
                    if (current == null) {
                        parent.right = node;
                        count++;
                        break;
                    }
                }
//                        else {
//                            error = "having same value in binary tree";
//                        }
            } // end of while
        }
    }

    protected String Judging_results;
    public boolean Judgenode(T Element) {
        BinaryTreeNode<T> node = new BinaryTreeNode<T>(Element);
        if (root == null){
            Judging_results = "It's not a leaf or an internal node .";
            return false;
        }
        else {
            if (root != null&&root.left==null&&root.right==null){
                Judging_results = "Both leaves and internal nodes .";
            }
           node = findNode(Element,root);
           if (node.left!=null||node.right!=null){
               Judging_results = "internal nodes .";
               return true;
           }
           else {
               Judging_results = "leaf nodes . ";
               return false;
           }
        }
    }

    public static void main(String[] args) {
        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
        System.out.println("树是否为空 ？" + linkedBinaryTree.isEmpty());
        System.out.println("往树中添加的元素有 ：4,2,3,54,23,16,67,22,36,48,12,42,33 。 ");
        linkedBinaryTree.add(4);
        linkedBinaryTree.add(2);
        linkedBinaryTree.add(3);
        linkedBinaryTree.add(54);
        linkedBinaryTree.add(23);
        linkedBinaryTree.add(16);
        linkedBinaryTree.add(67);
        linkedBinaryTree.add(22);
        linkedBinaryTree.add(36);
        linkedBinaryTree.add(48);
        linkedBinaryTree.add(12);
        linkedBinaryTree.add(42);
        linkedBinaryTree.add(33);
        System.out.println("树是否为空 ？" + linkedBinaryTree.isEmpty());
        System.out.println("打印树：");
        linkedBinaryTree.showTree();
        System.out.println("树的高度为：" + linkedBinaryTree.getHeight());
        System.out.println("树中的元素个数为 ：" + linkedBinaryTree.size());
        System.out.println("元素23是否在树内 ？ " + linkedBinaryTree.contains(23));
        System.out.println("元素9是否在树内 ？" + linkedBinaryTree.contains(9));
        System.out.println("删去右子树后 ：");
        linkedBinaryTree.removeRightsubtree();
        linkedBinaryTree.showTree();
        System.out.println("往树中添加元素 ：3,29,49,17,82,33,21,6,9 ");
        linkedBinaryTree.add(5);
        linkedBinaryTree.add(29);
        linkedBinaryTree.add(49);
        linkedBinaryTree.add(17);
        linkedBinaryTree.add(82);
        linkedBinaryTree.add(33);
        linkedBinaryTree.add(21);
        linkedBinaryTree.add(6);
        linkedBinaryTree.add(9);
        linkedBinaryTree.showTree();
        System.out.println("判断33是叶子还是内部结点 ：");
//        System.out.println(linkedBinaryTree.getHeight());
//        System.out.println(linkedBinaryTree.size());
//        System.out.println(linkedBinaryTree.contains(23));
////        linkedBinaryTree.removeNodeLeftsubtree(23);
//        linkedBinaryTree.showTree();
        linkedBinaryTree.Judgenode(33);
        System.out.println(linkedBinaryTree.Judging_results);
        System.out.println("判断6是叶子还是内部结点 ： ");
//        BinaryTreeNode current = new BinaryTreeNode(23);
//        linkedBinaryTree.removeNodeLeftsubtree(current,23,1);
//        linkedBinaryTree.removeAllElements();
//        linkedBinaryTree.showTree();
//        linkedBinaryTree.removeLeftsubtree();
//        linkedBinaryTree.showTree();
//        linkedBinaryTree.removeRightsubtree();
        linkedBinaryTree.Judgenode(6);
        System.out.println(linkedBinaryTree.Judging_results);
        System.out.println("删除元素17的左子树：");
        linkedBinaryTree.removeNodeLeftsubtree(17);
        linkedBinaryTree.showTree();
        Iterator iterator =  linkedBinaryTree.iteratorInOrder();
        while (iterator.hasNext()){
            System.out.print(iterator.next() + " ");
        }
    }
}

