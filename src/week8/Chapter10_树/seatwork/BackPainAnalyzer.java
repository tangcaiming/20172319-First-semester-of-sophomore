package week8.Chapter10_树.seatwork;


import week8.Chapter10_树.seatwork.DecisionTree;
import java.io.FileNotFoundException;

/**
 * BackPainAnaylyzer demonstrates the use of a binary decision tree to 
 * diagnose back pain.
 *
 树的深度和叶子个数计算：
 * （1）参考下面叶子节点计算的伪代码，计算课本中背部疼痛诊断器中决策树的叶子节点个数？
 int CountLeaf ( BiTree T,  int &count )
 {     if ( T )
 {     if (  (!T->lchild) && (!T->rchild)  )
 count++;     // 对叶子结点计数
 CountLeaf( T->lchild, count);
 CountLeaf( T->rchild, count);
 }
 }
 （2）根据课堂介绍的递归树深度计算算法，计算决策树的深度。
 注：本次4分

 层次遍历法实践：
 （1）使用递归实现层次遍历背部疼痛决策树，并按照层次顺序输出每个节点内容。（2分）
 （2）非递归的层次遍历法算法如下：
 根结点入队；
 从队头取元素，   并执行如下3个动作：
 (1)访问该结点；
 (2)如果该元素有左孩子，则左孩子入队；
 (3)如果该元素有右孩子，则右孩子入队；
 重复执行上述过程，直至队列为空。   此时，二叉树遍历结束。
 按照上述算法，编程实现层序遍历，按照层序的方法，遍历并依次输出每个节点内容。（3分）

 本次满分为5分。
 */
public class BackPainAnalyzer
{
    /**
     *  Asks questions of the user to diagnose a medical problem.
     */
    public static void main (String[] args) throws FileNotFoundException
    {
        System.out.println ("So, you're having back pain.");

//        try {
//            File file = new File("D:\\huawei\\Javawindows文件","input.txt");
//            InputStreamReader reader = new InputStreamReader(new FileInputStream(file));
//            BufferedReader bufferedReader = new BufferedReader(reader);

            DecisionTree expert = new DecisionTree("input.txt");
            expert.evaluate();
            expert.showtree();
            System.out.println("树的叶子结点个数为： " + expert.getCountLeaf());;
            System.out.println("树的深度为： " + expert.getheight());
            System.out.println("调用递归方法实现的层级遍历打印结点： ");
            expert.LevelOrder_recursion();
            System.out.println("调用非递归方法实现的层级遍历打印结点： ");
            expert.LevelOrder_norecursion();
//        }
//        catch (IOException E){
//            System.out.println("错误，指定路径不存在");
//        }
    }
}
