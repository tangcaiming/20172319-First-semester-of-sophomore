package week8.Chapter10_树.seatwork;



import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The DecisionTree class uses the LinkedBinaryTree class to implement 
 * a binary decision tree. Tree elements are read from a given file and  
 * then the decision tree can be evaluated based on user input using the
 * evaluate method. 
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class DecisionTree
{
    private LinkedBinaryTree<String> tree;
    
    /**
     * Builds the decision tree based on the contents of the given file
     *
     * @param filename the name of the input file
     * @throws FileNotFoundException if the input file is not found
     */
    public DecisionTree(String filename) throws FileNotFoundException
    {
        File inputFile = new File(filename);
        Scanner scan = new Scanner(inputFile);
        int numberNodes = scan.nextInt();
        scan.nextLine();
        int root = 0, left, right;
        
        List<LinkedBinaryTree<String>> nodes = new ArrayList<LinkedBinaryTree<String>>();
        for (int i = 0; i < numberNodes; i++) {
            nodes.add(i,new LinkedBinaryTree<String>(scan.nextLine()));
        }
        
        while (scan.hasNext())
        {
            root = scan.nextInt();
            left = scan.nextInt();
            right = scan.nextInt();
            scan.nextLine();
            
            nodes.set(root, new LinkedBinaryTree<String>((nodes.get(root)).getRootElement(), 
                                                       nodes.get(left), nodes.get(right)));
        }
        tree = nodes.get(root);
    }

    /**
     *  Follows the decision tree based on user responses.
     */
    public void evaluate()
    {
        LinkedBinaryTree<String> current = tree;
        Scanner scan = new Scanner(System.in);

        while (current.size() > 1)
        {
            System.out.println (current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N")) {
                current = current.getLeft();
            } else {
                current = current.getRight();
            }
        }

        System.out.println (current.getRootElement());
    }
    // 获得叶子个数
    public int getCountLeaf(){
        return tree.CountLeaf(tree.getRoot(),0);
    }
    // 获取树高度
    public int getheight(){
        return tree.getHeight();
    }
    // 打印决定树
    public void showtree(){
         System.out.println(tree.printTree());
    }
    //  递归实现层级遍历
    public void LevelOrder_recursion(){
        tree.LevelOrder_recursion(tree.getRoot());
    }
    //  非递归实现层级遍历
    public void LevelOrder_norecursion(){
        tree.LevelOrder_norecursion();
    }
}