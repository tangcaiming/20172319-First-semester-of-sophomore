package week2.Chapter3;

import java.util.Scanner;
import java.util.Stack;

/**
 * PP项目3.2
 * 读取输入句子，反向显示每个词的字符，用栈来颠倒每个词的字符
 *
 * @author 唐才铭
 * @time 2018-09-16
 */
public class Stack_implements_the_reverse_output_string {
    public static void main(String[] args) {
        System.out.println("Enter your sentence:");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] Word = new String[10];
        Stack<Character> topsy_turvy = new Stack<>();
        Stack<String> No_topsy_turvy = new Stack<String>();
        for (int i = 0; i < input.length(); i++) {
            topsy_turvy.push(input.charAt(i));
        }
        Word = input.split(" ");
        for (int j = 0; j < Word.length; j++) {
            No_topsy_turvy.push(Word[j]);
        }
        System.out.println("Which one? (No_topsy_turvy(0)||topsy_turvy(1))");
        int choose = scanner.nextInt();
        if (choose == 1) {
            while (!topsy_turvy.isEmpty()) {
                System.out.print(topsy_turvy.pop());
            }
        } else {
            while (!No_topsy_turvy.isEmpty()) {
                System.out.print(No_topsy_turvy.pop()+ " ");
            }
        }
    }
}
