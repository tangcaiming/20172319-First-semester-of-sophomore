package week2.Chapter3;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/**
 * PP项目3.8
 * 用栈将用户输入的元素集反顺序输出
 *
 * @author 唐才铭
 * @time 2018-09-16
 */
public class reverse {
    public static void main(String[] args) {
        System.out.println("Enter your elements in collections:");
        Scanner scanner = new Scanner(System.in);
        List elements = new LinkedList();
        while (scanner.hasNextInt()){
            int element = scanner.nextInt();
            elements.add(String.valueOf(element));
            scanner.hasNext();
        }
        System.out.println("Your collections:");
        System.out.println(elements);
        Stack collections = new Stack();
        for (int i = 0 ; i < elements.size(); i++){
            collections.push(elements.get(i));
        }
        while (!elements.isEmpty()){
            elements.clear();
        }
        while (!collections.isEmpty()){
            elements.add(collections.pop());
        }
        System.out.println("Your new collections:");
        System.out.println(elements);
    }
}
