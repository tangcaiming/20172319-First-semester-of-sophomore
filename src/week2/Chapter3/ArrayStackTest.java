package week2.Chapter3;

/**
 * 对ArrayStack类进行测试
 *
 * 课堂作业
 * @author 唐才铭
 * 2018 09 12
 * 22:45
 */
public class ArrayStackTest {
    public static void main(String[] args) {
        ArrayStack arrayStack = new ArrayStack();
        arrayStack.push(20172301);
        arrayStack.push(20172302);
        arrayStack.push(20172303);
        arrayStack.push(20172304);
        arrayStack.push(20172305);
        arrayStack.push(20172306);

        System.out.println("When the data is pushed:");
        System.out.println("The number of elements of the stack is :" + "\t"+arrayStack.size());
        System.out.println("Whether the stack is empty ?: " + "\t"+arrayStack.isEmpty());
        System.out.println("The situation inside the stack is: " + "\t" + arrayStack.toString());
        System.out.println("The element at the top of the stack is: " + "\t"+arrayStack.peek());
        arrayStack.pop();
        arrayStack.pop();
        arrayStack.pop();
        arrayStack.pop();
        arrayStack.pop();
        arrayStack.pop();
        System.out.println();
        System.out.println("When the data is out of the stack");
        System.out.println("The number of elements of the stack is :" + "\t"+arrayStack.size());
        System.out.println("Whether the stack is empty ?: " + "\t"+arrayStack.isEmpty());
        System.out.println("The situation inside the stack is: " + "\t" +arrayStack.toString());
        System.out.println("The element at the top of the stack is: " + "\t"+ arrayStack.peek());


    }
}
