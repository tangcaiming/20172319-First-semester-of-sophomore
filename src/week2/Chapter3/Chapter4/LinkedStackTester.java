package week2.Chapter3.Chapter4;

public class LinkedStackTester {
    public static void main(String[] args) {
        LinkedStack linkedStack = new LinkedStack();
        linkedStack.push("20172301");
        linkedStack.push("20172302");
        linkedStack.push("20172303");
        linkedStack.push("20172304");
        linkedStack.push("20172305");
        linkedStack.push("20172306");
        linkedStack.push("20172307");
        System.out.println(linkedStack);
        linkedStack.pop();
        System.out.println(linkedStack);
        System.out.println(linkedStack.isEmpty());
        System.out.println(linkedStack.size());
        System.out.println(linkedStack.peek());
        linkedStack.clear();
        System.out.println(linkedStack);
    }
}
