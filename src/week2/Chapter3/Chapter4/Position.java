package week2.Chapter3.Chapter4;

public class Position {
    private int x;
    private int y;

    public Position() {

    }

    public void setx(int x) {
        this.x = x;
    }

    public void sety(int n2) {
        this.y = n2;
    }

    public int getx() {
        return x;
    }

    public int gety() {
        return y;
    }

}
