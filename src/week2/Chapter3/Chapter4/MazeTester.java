package week2.Chapter3.Chapter4;

import java.util.*;
import java.io.*;

/**
 * MazeTester uses recursion to determine if a maze can be traversed.
 *
 *@author Lewis and Chase
 *@version 4.0
 */
public class MazeTester {
    public static void main(String[] args) throws FileNotFoundException {
        /**
         * Creates a new maze, prints its original form, attemps to
         * solve it, and prints out its final form.
         */
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the name of the file containing the maze:");
        String filename = scanner.nextLine() ;

        Maze labyrinth = new Maze(filename);

        System.out.println(labyrinth);

        MazeSolver solver = new MazeSolver(labyrinth);
        if(solver .traverse() ) {
            System.out.println("the maze was successfully traversed");
        } else {
            System.out.println("there is no possible path.");
        }

        System.out.println(labyrinth);
    }

}
