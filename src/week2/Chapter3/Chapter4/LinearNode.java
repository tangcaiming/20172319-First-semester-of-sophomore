package week2.Chapter3.Chapter4;

/**
 * Represents a node in a linked list.
 *
 * @author Lewis and Chase
 * @param <T>
 */
public class LinearNode<T> {
    private LinearNode<T> next;
    private T elements;

    /**
     * Creates an empty node.
     */
    public LinearNode() {
        next = null;
        elements = null;
    }

    /**
     * Craeates a node storing the specified element.
     * @param elem element to be stored.
     */
    public LinearNode (T elem) {
        next = null;
        elements = elem;
    }

    /**
     * Returns the node that follows this one.
     * @return LinearNode<T></> reference to next node
     */
    public LinearNode<T> getNext() {
        return next;
    }

    /**
     * Sets the node that follwos this one
     * @param node node to follow this one
     */
    public void setNext(LinearNode<T> node) {
        next = node;
    }

    /**
     * Returns the element stored in this node.
     * @return T element stored at this node.
     */
    public T getElements() {
        return elements;
    }

    /**
     * Sets the element stored in this node.
     * @param elem element to be stored at this node
     */
    public void setElements(T elem) {
        elements = elem;
    }
}
