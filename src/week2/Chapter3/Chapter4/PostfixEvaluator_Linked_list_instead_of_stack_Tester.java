package week2.Chapter3.Chapter4;



import java.util.Scanner;

/**
 * PP项目4.2
 * 修改第3章的postfix程序，使其使用LinkedStack<T>类而不是ArrayStack<T>类</>
 *
 * @author 唐才铭
 * @time 2018-09-16
 */
public class PostfixEvaluator_Linked_list_instead_of_stack_Tester {
    /**
     * Reads and evaluates multiple postfix expressions.
     */
    public static void main(String[] args) {
        String expression,again;
        int result;

        Scanner in = new Scanner(System.in);

        do {
            PostfixEvaluator_Linked_list_instead_of_stack evaluator = new PostfixEvaluator_Linked_list_instead_of_stack();
            System.out.println("Enter a vaild post-fix expression one token" + "at a time with a space between each token(e.g. 5 5 + 3 2 1 - + *)");
            System.out.println("Each token must be an integer or an operator(+,-,*,/)");
            expression = in.nextLine();

            result = evaluator.evaluate(expression);
            System.out.println();
            System.out.println("That expression eauals " + result);
            System.out.println("Evaluate anothor expression [Y/N]? ");
            again = in.nextLine();
            System.out.println();
        }
        while(again.equalsIgnoreCase("y"));
    }
}
