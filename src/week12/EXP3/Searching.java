package week12.EXP3;

public class Searching {
    /**
     * Searches the specified array of objects using a linear search
     * algorithm.
     *
     * @param data   the array to be searched
     * @param min    the integer representation of the minimum value
     * @param max    the integer representation of the maximum value
     * @param target the element being searched for
     * @return       true if the desired element is found
     */
    public static <T> boolean linearSearch(T[] data, int min, int max, T target)
    {
        int index = min;
        boolean found = false;
        while (!found && index <= max)
        {
            found = data[index].equals(target);
            index++;
        }
        return found;
    }
}
