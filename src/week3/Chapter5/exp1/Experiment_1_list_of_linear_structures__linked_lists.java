package week3.Chapter5.exp1;


import java.io.*;
import java.util.Scanner;
import java.util.Stack;

/**
 * 实验一
 * 链表练习，要求实现下列功能：
 * （1）通过键盘输入一些整数，建立一个链表（1分）；
 这些数是你学号中依次取出的两位数。  再加上今天的时间。
 例如你的学号是 20172301
 今天时间是 2018/10/1， 16：23：49秒
 数字就是
 20， 17，23，1， 20， 18，10，1，16，23，49
 打印所有链表元素， 并输出元素的总数。
 在你的程序中，请用一个特殊变量名来纪录元素的总数，变量名就是你的名字。 例如你叫 张三， 那么这个变量名就是
 int nZhangSan = 0;   //初始化为 0.
 （2）实现节点插入、删除、输出操作（2分，3个知识点根据实际情况酌情扣分）；
 继续你上一个程序， 扩展它的功能，每做完一个新功能，或者写了超过10行新代码，就签入代码，提交到源代码服务器；
 从磁盘读取一个文件， 这个文件有两个数字。
 从文件中读入数字1，  插入到链表第 5 位，并打印所有数字，和元素的总数。 保留这个链表，继续下面的操作。
 从文件中读入数字2， 插入到链表第 0 位，并打印所有数字，和元素的总数。 保留这个链表，并继续下面的操作。
 从链表中删除刚才的数字1.  并打印所有数字和元素的总数。
 （3）使用冒泡排序法或者选择排序法根据数值大小对链表进行排序（2分）；
 如果你学号是单数， 选择冒泡排序， 否则选择选择排序。
 在排序的每一个轮次中， 打印元素的总数，和目前链表的所有元素。
 在（2）得到的程序中继续扩展， 用同一个程序文件，写不同的函数来实现这个功能。 仍然用 nZhangSan （你的名字）来表示元素的总数。
 * @author 唐才铭
 * @time2018-09-28
 * 20 17 23 19 20 18 09 28 11 04 29
 */
public class Experiment_1_list_of_linear_structures__linked_lists {
    public static void main(String[] args) throws Exception{
        System.out.println("实验的第一部分：");
        System.out.print("Enter some integers and create a linked list :   ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] strings = input.split(" ");
//        strings = input.split(" ");
        Stack<String> Break_up = new Stack<String>();
        for (int i = strings.length; i > 0 ; i--){
            Break_up.push(strings[i-1]);
        }
        System.out.print("The contents of the stack are :   ");
        System.out.println(Break_up);
        Linked_list linked_list = new Linked_list();
        linked_list.add(0);
        while (!Break_up.empty()) {
            int tempelement = Integer.parseInt(Break_up.pop());
            linked_list.add(tempelement);
        }
        int ntangcaiming = 0;
        ntangcaiming = linked_list.getCount();
        System.out.print("The contents of the queue are :   ");
        System.out.println(linked_list);
        System.out.print("The number of linked elements is :   ");
        System.out.println(ntangcaiming);
        System.out.println("实验的第二部分：");
        try {
            File file = new File("D:\\huawei\\Javawindows文件","EXP1-First semester of sophomore.txt");
//            if(!file.exists()){
//                file.createNewFile();
//            }
//            Writer writer = new FileWriter(file);
//            writer.write(1);
//            writer.write("");
//            writer.write(2);
//            writer.flush();
            InputStreamReader reader = new InputStreamReader(new FileInputStream(file));
            BufferedReader bufferedReader = new BufferedReader(reader);
            int[] file_word_temp = new int[2];
            String[] file_word = bufferedReader.readLine().split(" ");
            file_word_temp[0] = Integer.parseInt(file_word[0]);
            file_word_temp[1] = Integer.parseInt(file_word[1]);

            Linked_list_node Node_insert1 = new Linked_list_node(file_word_temp[0]);
            Linked_list_node Node_insert2 = new Linked_list_node(file_word_temp[1]);

            linked_list.insert(5,Node_insert1);
            System.out.print("The list after inserting 1 at the fifth position is ：   ");
            System.out.println(linked_list);
            System.out.print("The number of linked elements is :   ");
            ntangcaiming = linked_list.getCount();
            System.out.println(ntangcaiming);
            linked_list.insert(1,Node_insert2);
            System.out.print("The list after inserting 2 at the first position is ：   ");
            System.out.println(linked_list);
            ntangcaiming = linked_list.getCount();
            System.out.print("The number of linked elements is :   ");
            System.out.println(ntangcaiming);
            System.out.print("The list after deleting the inserted number 1 is ：   ");
            linked_list.delete(6);
            System.out.println(linked_list);
            ntangcaiming = linked_list.getCount();
            System.out.print("The number of linked elements is :   ");
            System.out.println(ntangcaiming);
            System.out.println("实验的第三部分：");
            System.out.println("Print only the rounds that have implemented the element exchange:");

            linked_list.Bubble_sort(linked_list.head,linked_list);
//            System.out.println(linked_list);
//            ntangcaiming = linked_list.getCount();
//            System.out.print("The number of linked elements is :   ");
//            System.out.println(ntangcaiming);
    }
        catch (IOException E){
        System.out.println("错误，指定路径不存在");
    }
//        System.out.println("Enter the number for the following operation:");
//        System.out.println("Insert(1), delete(2), output(3)");
//        int choice = scanner.nextInt();
//        if (choice==1){
//            System.out.println("Enter the node location you want to insert:");
//            int location_insert = scanner.nextInt();
//            System.out.println("Enter the element you want to insert at the node:");
//            int element = scanner.nextInt();
//            Linked_list_node Node_insert = new Linked_list_node(element);
//            linked_list.insert(location_insert,Node_insert);
//        }
//        else
//            if (choice==2){
//            System.out.println("Enter the node location you want to delete:");
//            int location_delete = scanner.nextInt();
//            linked_list.delete(location_delete);
//
//        }
//            else {
//
//        }
//        System.out.println(linked_list);
    }
}

