package week3.Chapter5.exp1;

public class Linked_list {
    protected Linked_list linked_list;
    protected Linked_list_node head ;
    protected int count;
    private Linked_list_node node ;
    protected Linked_list_node sentinel = new Linked_list_node(0);


    public void add(int number){
        Linked_list_node Node = new Linked_list_node(number);
//        sentinel.next = head;
//        head = sentinel;
        if (this.head==null){
            this.head = Node;
        }
        else {
            this.head.addLinked_list_node(Node);
        }
    }

    public void insert(int index,Linked_list_node node){
        if(index < 1||index > getCount() + 1){
            System.out.println("Wrong position, cannot insert");
            return;
        }
        int length = 1;
        Linked_list_node temp = head;
        while(head.next != null)
        {
            if(index == length++){
                node.next = temp.next;
                temp.next = node;
                return;
            }
            temp = temp.next;
        }
    }

    public void delete(int index){
        if(index < 1 || index > getCount()){
            System.out.println("Wrong position, cannot be deleted");
            return;
        }
        int length=1;
        Linked_list_node temp = head;
        while(temp.next != null){
            if(index == length++){
                temp.next = temp.next.next;
                return;
            }
            temp = temp.next;
        }
    }

    public int getCount() {
        Linked_list_node temp = head;
        count = 0;
        while(temp.next != null){
            count++;
            temp = temp.next;
        }
        return count;
    }

    @Override
    public String toString() {
        String result = "";
        node = head;
        for (int i = 0 ; i < getCount(); i++){
            result += node.next.getNumber()+ " ";
            node = node.getNext();
        }
        return result;
    }

    public void Bubble_sort(Linked_list_node Head,Linked_list linked_list){
//        String result = "",line = "The list sorted by this bubbling sort is :  ", size = "The number of linked elements is :   ";

        Linked_list_node temp = null, tail = null;
        if(head == null || head.next == null)
        {
//           return linked_list;
        }
        temp = head;

        int count=1;
        while(temp.next != tail){
            while(temp.next != tail){
                if(temp.number > temp.next.number){
                    int temp_number = temp.number;
                    temp.number = temp.next.number;
                    temp.next.number = temp_number;
                    System.out.print("The list sorted by the "+ count + " truly bubbling sort is :  ");
                    System.out.println(linked_list);
                    System.out.print("The number of linked elements is :  " + linked_list.getCount() + "\n" );

                    count++;
                }
                temp = temp.next;

            }
            tail = temp;
            temp = head;
        }
//        return linked_list;
    }

//    public void Bubble_sort(Linked_list linked_list){
//        Linked_list_node temp = linked_list.head;
//        int temp_number = 0;
//        for (int i = 0 ; i < linked_list.getCount(); i++){
//            for (int j = 0 ; j < linked_list.getCount() - i - 1; j++) {
//                if (temp.number > temp.next.number) {
//                    temp_number = temp.number;
//                    temp.setNumber(temp.next.number);
//                    temp.next.setNumber(temp_number);
//                }
//                temp = temp.next;
//                if (temp.next==null){
//                    break;
//                }
//            }
//            if (temp.next==null){
//                break;
//            }
//        }
//    }
}
