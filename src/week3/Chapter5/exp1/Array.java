package week3.Chapter5.exp1;

public class Array {
    protected String[] MyArray;
    public Array(String[] MyArray){
        this.MyArray = MyArray;
    }
    public int size(){
        return MyArray.length;
    }

    public String[] Array_Insert(int index,String number){
        String[] newMyArray = new String[MyArray.length+1];
        if (index<0||index>MyArray.length){
            System.out.println("Error, this location does not exist.");
        }
        else {
            newMyArray[index] = number;
            for (int i = 0; i < index; i++){
                newMyArray[i] = MyArray[i];
            }
            for (int i = index + 1; i < newMyArray.length; i++){
                newMyArray[i] = MyArray[i-1];
            }
        }
        return newMyArray;
    }
    public String[] Array_Delete(int index){
        String[] newMyArray = new String[MyArray.length-1];
        if (index<0||index>MyArray.length){
            System.out.println("Error, this location does not exist.");
        }
        else {
            for (int i = 0; i < index ; i++){
                newMyArray[i] = MyArray[i];
            }
            for (int i = index ; i < newMyArray.length; i++){
                newMyArray[i] = MyArray[i+1];
            }
        }
        return newMyArray;
    }

    public String Array_Selection_sort() {
        int[] temp_MyArray = new int[MyArray.length];
        for (int i = 0 ; i < MyArray.length; i ++){
            temp_MyArray[i] = Integer.parseInt(MyArray[i]);
        }

        String result = "";
        for (int i = 0; i < temp_MyArray.length - 1 ; i++){
            for (int j = i + 1;j < temp_MyArray.length; j++ ){
                if (temp_MyArray[i]<temp_MyArray[j]){
                    int temp = temp_MyArray[i];
                    temp_MyArray[i] = temp_MyArray[j];
                    temp_MyArray[j] = temp;
                    String every = "";
                    for (int data : temp_MyArray){
                        every += data + " ";
                    }
                    result += "The list sorted by the SelectSorting is :  " + every + "\n" +
                            "The number of elements in the array is:  :" + MyArray.length + "\n";
                }
            }
        }
        return result;
    }

    @Override
    public String toString() {
        String result = "";
        for (String string : MyArray){
            result += string + " ";
        }
        return result;
    }
}
