package week3.Chapter5.exp1;

public class Linked_list_node {
    protected int number;
    protected Linked_list_node next;
    public Linked_list_node (int number){
        this.number = number;
        this.next = null;
    }
    public void addLinked_list_node(Linked_list_node Node){
        if (this.next==null){
            this.next = Node;
        }
        else {
            this.next.addLinked_list_node(Node);
        }
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public int getNumber() {
        return this.number;
    }
    public void setNext(Linked_list_node next) {
        this.next = next;
    }
    public Linked_list_node getNext() {
        return this.next;
    }
    @Override
    public String toString() {
        return String.valueOf(this.number);
    }
}
