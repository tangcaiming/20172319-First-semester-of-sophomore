package week3.Chapter5.exp1;

import week3.Chapter5.jsjf.LinkedQueue;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;



/**
 * 实验一
 数组练习，要求实现下列功能：

 （1）通过键盘输入一些整数，建立一个链表（1分）；
 这些数是你学号中依次取出的两位数。  再加上今天的时间。
 例如你的学号是 20172301
 今天时间是 2018/10/1， 16：23：49秒
 数字就是
 20， 17，23，1， 20， 18，10，1，16，23，49
 打印所有数组元素， 并输出元素的总数。
 在你的程序中，请用一个特殊变量名来纪录元素的总数，变量名就是你的名字。 例如你叫 张三， 那么这个变量名就是
 int nZhangSan = 0;   //初始化为 0.
 做完这一步，把你的程序签入源代码控制（git push）。
 （2）实现节点插入、删除、输出操作（2分，3个知识点根据实际情况酌情扣分）；
 继续你上一个程序， 扩展它的功能，每做完一个新功能，或者写了超过10行新代码，就签入代码，提交到源代码服务器；
 从磁盘读取一个文件， 这个文件有两个数字。
 从文件中读入数字1，  插入到数组第 5 位，并打印所有数字，和元素的总数。 保留这个数组，继续下面的操作。
 从文件中读入数字2， 插入到数组第 0 位，并打印所有数字，和元素的总数。 保留这个数组，并继续下面的操作。
 从数组中删除刚才的数字1.  并打印所有数字和元素的总数。

 签入所有代码。
 数组练习，要求实现下列功能：
 （3）使用冒泡排序法或者选择排序法根据数值大小对数组进行排序（2分）；
 如果你学号是单数， 选择选择排序， 否则选择冒泡排序。
 在排序的每一个轮次中， 打印元素的总数，和目前数组的所有元素。

 在（2）得到的程序中继续扩展， 用同一个程序文件，写不同的函数来实现这个功能。 仍然用 nZhangSan （你的名字）来表示元素的总数。

 *@author 唐才铭
 *@time 2018-09-28
 * 20 17 23 19 20 18 09 28 11 04 29
 */
public class Experiment_1_list_of_linear_structures__arrays {
    protected String[] newMyArray;
    public Experiment_1_list_of_linear_structures__arrays(String[] MyArray){
        this.newMyArray = MyArray;
    }
    public static void main(String[] args) {
        System.out.println("实验的第一部分：");
        System.out.print("Enter some integers and create a linked list:");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        int ntangcaiming = 0 ;
        String[] temp_MyArray = input.split(" ");
        Array MyArray = new Array(temp_MyArray);
//        for (int i = 0 ;i<strings.length;i++){
//            myarray.add(Integer.parseInt(strings[i]));
//        }
//        for (int i = strings.length; i > 0 ; i--){
//            myarray.add(Integer.parseInt(strings[i-1]));
//        }
        System.out.print("The elements in the array are:   ");
        System.out.println(MyArray);
        System.out.print("The number of elements in the array is:  ");
        ntangcaiming = MyArray.size();
        System.out.println(ntangcaiming);
        System.out.println("实验的第二部分：");
        try {
            File file = new File("D:\\huawei\\Javawindows文件","EXP1-First semester of sophomore.txt");
            InputStreamReader reader = new InputStreamReader(new FileInputStream(file));
            BufferedReader bufferedReader = new BufferedReader(reader);
            int[] file_word_temp = new int[2];
            String[] file_word = bufferedReader.readLine().split(" ");
            file_word_temp[0] = Integer.parseInt(file_word[0]);
            file_word_temp[1] = Integer.parseInt(file_word[1]);


            System.out.print("The array after 1 is inserted in position 5 is ：   ");
            Array MyArray1 = new Array(MyArray.Array_Insert(4, String.valueOf(file_word_temp[0]))) ;
            System.out.println(MyArray1);
            System.out.print("The number of elements in the array is:  ");
            ntangcaiming = MyArray1.size();
            System.out.println(ntangcaiming);


            System.out.print("The list after inserting 2 at the first position is ：   ");
//            String[] newMyArray1 = new String[newMyArray.length+1];
            Array MyArray2 = new Array(MyArray1.Array_Insert(0, String.valueOf(file_word_temp[1])));
            System.out.println(MyArray2);
            System.out.print("The number of elements in the array is:  ");
            ntangcaiming = MyArray2.size();
            System.out.println(ntangcaiming);

            System.out.print("The array after deleting the inserted number 1 is ：   ");
//            String[] newMyArray2 = new String[newMyArray.length-1];
            Array MyArray3 = new Array(MyArray2.Array_Delete(5));
            System.out.println(MyArray3);
            System.out.print("The number of elements in the array is:  ");
            ntangcaiming = MyArray3.size();
            System.out.println(ntangcaiming);
            System.out.println("实验的第三部分：");
//            int[] temp_MyArray3 = new int[MyArray3.size()];
//            for (int i = 0; i < temp_MyArray3.length; i++){
//                temp_MyArray3[i] = Integer.parseInt(MyArray3[i]);
//            }
            System.out.print(MyArray3.Array_Selection_sort());
//            System.out.println("Print only the rounds that have implemented the element exchange:");
//
//            linked_list.Bubble_sort(linked_list.head,linked_list);
//            System.out.println(linked_list);
//            ntangcaiming = linked_list.getCount();
//            System.out.print("The number of linked elements is :   ");
//            System.out.println(ntangcaiming);
        }
        catch (IOException E){
            System.out.println("错误，指定路径不存在");
        }
    }
}
