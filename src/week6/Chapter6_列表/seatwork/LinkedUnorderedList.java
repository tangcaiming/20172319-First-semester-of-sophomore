package week6.Chapter6_列表.seatwork;


import week6.Chapter6_列表.jsjf.LinearNode;
import week6.Chapter6_列表.jsjf.LinkedList;
import week6.Chapter6_列表.jsjf.UnorderedListADT;

/**
 * LinkedUnorderedList represents a singly linked implementation of an
 * unordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedUnorderedList<T> extends LinkedList<T>
         implements UnorderedListADT<T>
{
    /**
     * Creates an empty list.
     */
    public LinkedUnorderedList()
    {
        super();
    }

    /**
     * Adds the specified element to the front of this list.
     *
     * @param element the element to be added to the list
	 */
    @Override
    public void addToFront(T element)
    {
        // To be completed as a Programming Project
        LinearNode<T> current = new LinearNode<T>((T) element);
        if (head == null) {
            head = current;
            tail = current;
        }
        else {
            current.setNext(head);
            head = current;
        }
        count++;
        modCount++;
    }

	/**
     * Adds the specified element to the rear of this list.
     *
     * @param element the element to be added to the list
	 */
    @Override
    public void addToRear(T element)
    {
        // To be completed as a Programming Project
        LinearNode current = new LinearNode(element);
        if (isEmpty()){
            head = current;
        }
        else {
            tail.setNext(current);
        }
        tail = current;
        count++;
        modCount++;
    }


    /**
     * Adds the specified element to this list after the given target.
     *
     * @param  element the element to be added to this list
	 * @param  target the target element to be added after
	 * @throwsElementNotFoundException if the target is not found
	 */
    @Override
    public void addAfter(T element, T target)
    {
        // To be completed as a Programming Project
        LinearNode<T> node = new LinearNode<>(element);
        LinearNode<T> current = head;
        LinearNode<T> temp = current.getNext();

        for (int i = 1;i < size();i++){
            if (target.equals(current.getElement())) {
                node.setNext(temp);
                current.setNext(node);

            }
            current = temp;
            temp = temp.getNext();
        }
        count++;
        modCount++;
    }

    public int find(Product product){
        int scan = 0;
        int result = -1;
        LinearNode current = head;
        if (!isEmpty()){
            while (result == -1 && scan<size()){
                if (product.equals(current.getElement())){
                    result = scan + 1 ;
                }
                else {
                    current = current.getNext();
                    scan++;
                }
            }
        }

        return result;
    }

    public void Selection_Sorting(LinkedUnorderedList linkedUnorderedList) {
        if (isEmpty()) {
            System.out.println("");
        } else {
            LinearNode<Product> previous = null;
            LinearNode<Product> after = null;
//            LinearNode<Product> temp = null;
            for (previous = linkedUnorderedList.head; previous != null; previous = previous.getNext()) {
                for (after = previous.getNext(); after != null; after = after.getNext()) {
                    if (previous.getElement().Serial_number_commodity > after.getElement().Serial_number_commodity) {
                        int number = after.getElement().Serial_number_commodity;
                        int price = after.getElement().Price_of_commodity;
                        String name = after.getElement().getName_of_commodity();
                        after.getElement().Serial_number_commodity = previous.getElement().Serial_number_commodity;
                        after.getElement().Price_of_commodity = previous.getElement().Price_of_commodity;
                        after.getElement().Name_of_commodity = previous.getElement().Name_of_commodity;
                        previous.getElement().Serial_number_commodity = number;
                        previous.getElement().Name_of_commodity = name;
                        previous.getElement().Price_of_commodity = price;
                    }
                }
            }
//            while (previous!=null){
//                while (after!=null){
//                    if (after.getElement().compareTo(previous.getElement())< 0){
//                        temp = previous;
//                        previous = after;
//                        after = temp;
//                    }
//                    after = after.getNext();
//                }
//                previous = previous.getNext();
//                after = (LinearNode<Product>)head;
//            }
//        }
        }
    }

    public static void main(String[] args) {
        LinkedUnorderedList linkedUnorderedList = new LinkedUnorderedList();
        linkedUnorderedList.addToFront(20172301);
        linkedUnorderedList.addToRear(20172306);

        linkedUnorderedList.addToRear(20172303);
        linkedUnorderedList.addToRear(20172308);
        linkedUnorderedList.addToRear(20172307);
        linkedUnorderedList.addToRear(20172305);

        linkedUnorderedList.addToRear(20172302);
        linkedUnorderedList.addToRear(20172309);
        linkedUnorderedList.addToRear(20172304);


        System.out.println("Is the ordered list empty? ");
        System.out.println(linkedUnorderedList.isEmpty());
        System.out.println("The elements in an unordered list are:  " );
        System.out.println(linkedUnorderedList);
        System.out.println("After the element of the unordered list is inserted into 20172302, the list element are:  ");
        linkedUnorderedList.addAfter(20172301,20172302);
        System.out.println(linkedUnorderedList);
        System.out.println("The first element in an unordered list is:  ");
        System.out.println(linkedUnorderedList.first());
        System.out.println("The last element in an unordered list is:  ");
        System.out.println(linkedUnorderedList.last());

//        System.out.println("Look for elements in an ordered list 20172305:");
//        System.out.println(linkedUnorderedList.find(20172305));
        System.out.println("The element in the ordered list after deleting the specified element(20172305) are  ");
        linkedUnorderedList.remove(20172305);
        System.out.println(linkedUnorderedList);
        System.out.println("The number of elements in an ordered list is:   ");
        System.out.println(linkedUnorderedList.size());
        System.out.println("The element in the ordered list after deleting the header element are:  " );
        linkedUnorderedList.removeFirst();
        System.out.println(linkedUnorderedList);
        System.out.println("The element in the ordered list after deleting the last position element are:  " );
        linkedUnorderedList.removeLast();
        System.out.println(linkedUnorderedList);


    }
}
