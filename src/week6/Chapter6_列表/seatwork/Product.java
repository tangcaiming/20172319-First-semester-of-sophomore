package week6.Chapter6_列表.seatwork;


/**
 * 商品类，保留商品基本信息
 *
 * @author 唐才铭
 * @time 2018-10-10 14:42
 */
public class Product implements Comparable<Product> {
    protected String Name_of_commodity;
    protected int Price_of_commodity;
    protected int Serial_number_commodity;
    public Product(String Name_of_commodity,int Price_of_commodity,int Serial_number_commodity){
        this.Name_of_commodity = Name_of_commodity;
        this.Price_of_commodity = Price_of_commodity;
        this.Serial_number_commodity = Serial_number_commodity;
    }

    @Override
    public int compareTo(Product o) {
        if (Name_of_commodity.compareTo(o.getName_of_commodity())<0){
            return -1;
        }
        else
            if (Name_of_commodity.compareTo(o.getName_of_commodity())>0){
            return 1;
            }
        else
            if (Price_of_commodity<o.getPrice_of_commodity()){
                return -1;
            }
            else
            if (Price_of_commodity>o.getPrice_of_commodity()){
                return 1;
            }
        else
            if (Serial_number_commodity < o.getSerial_number_commodity()){
            return -1;
            }
            else {
                return 1;
            }

    }

    public String getName_of_commodity() {
        return Name_of_commodity;
    }

    public void setName_of_commodity(String name_of_commodity) {
        Name_of_commodity = name_of_commodity;
    }

    public int getPrice_of_commodity() {
        return Price_of_commodity;
    }

    public void setPrice_of_commodity(int price_of_commodity) {
        Price_of_commodity = price_of_commodity;
    }

    public int getSerial_number_commodity() {
        return Serial_number_commodity;
    }

    public void setDate_of_manufacture_of_merchandise(int Serial_number_commodity) {
        this.Serial_number_commodity = Serial_number_commodity;
    }

    @Override
    public String toString() {
        String result ="";
        result = "\t" +  "Name of commodity is : " + Name_of_commodity + "\t" + "Price of commodity is: " +
                Price_of_commodity +"\t"
        + "Serial_number_commodity is: " + Serial_number_commodity + "\n\r";
        return result;
    }

    public static void main(String[] args) {
        Product product1 = new Product("Coca Cola",3,20170309);
        Product product2 = new Product("Pepsi Cola",5,20170506);
        Product product3 = new Product("Mirinda     ",2,20170123);
        Product product4 = new Product("Jasmine Tea",6,20180506);
        Product product5 = new Product("Deluxe Milk",8,20180206);
        Product product6 = new Product("Pocari Sweat",5,20180122);
        Product product7 = new Product("Coca Cola",7,20180309);
        Product product8 = new Product("Coca Cola",2,20170912);

        LinkedUnorderedList linkedUnorderedList = new LinkedUnorderedList();
        linkedUnorderedList.addToFront(product2);
        linkedUnorderedList.addToRear(product1);
        linkedUnorderedList.addToRear(product7);
        linkedUnorderedList.addToRear(product8);
        linkedUnorderedList.addToRear(product4);
        linkedUnorderedList.addToRear(product6);
        linkedUnorderedList.addToRear(product3);
        linkedUnorderedList.addToRear(product5);
        System.out.println("\t" + "打印列表元素：");
        System.out.println("\t" + "The sequence of production after maintenance of unordered list is : ");
        System.out.print(linkedUnorderedList);
        System.out.println("\t" + "测试查找方法：");
        System.out.print("\t" + "The location of the commodity Pocari Sweat in the search list is :  ");
        System.out.println(linkedUnorderedList.find(product6));
        System.out.println("\t" + "测试删除方法：");
        System.out.println("\t" + "The list after deleting the item whose serial number is 20180122 is :  ");
        linkedUnorderedList.remove(product6);
        System.out.println(linkedUnorderedList);
        System.out.println("\t" + "测试插入方法：");
        linkedUnorderedList.addAfter(product6,product3);
        System.out.println("\t" + "The list after inserting the item whose serial number is 20180122 after the item whose serial number is 20170123 is :");
        System.out.println(linkedUnorderedList);
        System.out.println("\t" + "测试选择排序方法：");

        System.out.println("\t" + "The list after sorting(Serial_number_commodity) is : ");
        linkedUnorderedList.Selection_Sorting(linkedUnorderedList);
        System.out.println(linkedUnorderedList);
    }
}
