package week6.Chapter6_列表.jsjf;


import week6.Chapter6_列表.jsjf.exceptions.NonComparableElementException;

/**
 * LinkedOrderedList represents a singly linked implementation of an
 * ordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedOrderedList<T> extends LinkedList<T> implements OrderedListADT<T> {
    /**
     * Creates an empty list.
     */
    public LinkedOrderedList() {
        super();
    }

    /**
     * Adds the specified element to this list at the location determined by
     * the element's natural ordering. Throws a NonComparableElementException
     * if the element is not comparable.
     *
     * @param element the element to be added to this list
     * @throws NonComparableElementException if the element is not comparable
     */
    @Override
    public void add(T element) throws NonComparableElementException {
        // To be completed as a Programming Project
        if (!(element instanceof Comparable)) {
            throw new NonComparableElementException("LinkedOrderedList");
        }

        Comparable<T> comparableElement = (Comparable<T>) element;
        LinearNode current_node = new LinearNode(comparableElement);
        LinearNode temp_list = head;
        for (int i = 1; i < size(); i++) {
            temp_list = temp_list.getNext();
        }

        if (size()==0) {
            head = current_node;
            count++;
        }
        else
            if (comparableElement.compareTo(head.getElement())<0) {
                current_node.setNext(head);
                head = current_node;
                count++;
            }
            else
                if (comparableElement.compareTo((T) temp_list.getElement())>0) {
                    temp_list.setNext(current_node);
                    count++;
                }
                else {
                    LinearNode previous = head;
                    LinearNode temp = previous.getNext();
                    for (int scan = 1;scan < size(); scan++){
                        if (comparableElement.compareTo((T)temp.getElement())<0){
                            current_node.setNext(temp) ;
                            previous.setNext(current_node);
                            count++;
                            break;
                        }
                        previous = temp;
                        temp = temp.getNext();
            }
        }
    }

    public static void main(String[] args) {
        LinkedOrderedList linkedOrderedList = new LinkedOrderedList();

        linkedOrderedList.add(20172305);
        linkedOrderedList.add(20172306);
        linkedOrderedList.add(20172302);
        linkedOrderedList.add(20172307);
        linkedOrderedList.add(20172304);
        linkedOrderedList.add(20172308);
        linkedOrderedList.add(20172309);
        linkedOrderedList.add(20172301);
        linkedOrderedList.add(20172303);

        System.out.println("Is the ordered list empty? ");
        System.out.println(linkedOrderedList.isEmpty());
        System.out.println("The elements in an ordered list are:  " );
        System.out.println(linkedOrderedList);
        System.out.println("The first element in an ordered list is:  ");
        System.out.println(linkedOrderedList.first());
        System.out.println("The last element in an ordered list is:  ");
        System.out.println(linkedOrderedList.last());
//        System.out.println("Look for elements in an ordered list 20172305:");
//        System.out.println(linkedOrderedList.(20172305));
        System.out.println("The element in the ordered list after deleting the specified element are  ");
        linkedOrderedList.remove(20172305);
        System.out.println(linkedOrderedList);
        System.out.println("The number of elements in an ordered list is:   ");
        System.out.println(linkedOrderedList.size());
        System.out.println("The element in the ordered list after deleting the header element are:  " );
        linkedOrderedList.removeFirst();
        System.out.println(linkedOrderedList);
        System.out.println("The element in the ordered list after deleting the last position element are:  " );
        linkedOrderedList.removeLast();
        System.out.println(linkedOrderedList);
    }
}
