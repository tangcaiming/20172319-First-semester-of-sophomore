package week6.Chapter6_列表.jsjf;


import week6.Chapter6_列表.jsjf.exceptions.NonComparableElementException;

/**
 * ArrayOrderedList represents an array implementation of an ordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class ArrayOrderedList<T> extends ArrayList<T>
         implements OrderedListADT<T>
{
    /**
     * Creates an empty list using the default capacity.
     */
    public ArrayOrderedList()
    {
        super();
    }

    /**
     * Creates an empty list using the specified capacity.
     *
     * @param initialCapacity the initial size of the list
     */
    public ArrayOrderedList(int initialCapacity)
    {
        super(initialCapacity);
    }

    /**
     * Adds the specified Comparable element to this list, keeping
     * the elements in sorted order.
     *
     * @param element the element to be added to the list
     */
    @Override
    public void add(T element)
    {
		if (!(element instanceof Comparable)) {
            throw new NonComparableElementException("OrderedList");
        }
		
		Comparable<T> comparableElement = (Comparable<T>)element;
        
		if (size() == list.length) {
            expandCapacity();
        }

        int scan = 0;  
		
		// find the insertion location
        while (scan < rear && comparableElement.compareTo(list[scan]) > 0) {
            scan++;
        }

		// shift existing elements up one
        for (int shift=rear; shift > scan; shift--) {
            list[shift] = list[shift-1];
        }

		// insert element
        list[scan] = element;
        rear++;
		modCount++;
    }

    public static void main(String[] args) {
        ArrayOrderedList arrayOrderedList = new ArrayOrderedList();
        arrayOrderedList.add(20172303);
        arrayOrderedList.add(20172306);
        arrayOrderedList.add(20172307);
        arrayOrderedList.add(20172304);
        arrayOrderedList.add(20172305);
        arrayOrderedList.add(20172301);
        arrayOrderedList.add(20172308);
        arrayOrderedList.add(20172309);
        arrayOrderedList.add(20172302);


        System.out.println("Is the ordered list empty? ");
        System.out.println(arrayOrderedList.isEmpty());
        System.out.println("The elements in an ordered list are:  " );
        System.out.println(arrayOrderedList);
        System.out.println("The first element in an ordered list is:  ");
        System.out.println(arrayOrderedList.first());
        System.out.println("The last element in an ordered list is:  ");
        System.out.println(arrayOrderedList.last());
        System.out.println("Look for elements in an ordered list 20172305:");
        System.out.println(arrayOrderedList.find(20172305));
        System.out.println("The element in the ordered list after deleting the specified element are  ");
        arrayOrderedList.remove(20172305);
        System.out.println(arrayOrderedList);
        System.out.println("The number of elements in an ordered list is:   ");
        System.out.println(arrayOrderedList.size());
        System.out.println("The element in the ordered list after deleting the header element are:  " );
        arrayOrderedList.removeFirst();
        System.out.println(arrayOrderedList);
        System.out.println("The element in the ordered list after deleting the last position element are:  " );
        arrayOrderedList.removeLast();
        System.out.println(arrayOrderedList);








    }
}
