package week12.EXP3;

import org.junit.Before;
import org.junit.Test;
import week12.EXP3.cn.edu.besti.cs1723.TCM2319.Searching;

import static org.junit.Assert.*;

public class SearchingTest {
    Comparable linearsearch[] = new Comparable[]{1,3,6,9,12,11,2319,4};
    Comparable binarysearch[] = new Comparable[]{1,3,4,6,9,11,12,2319};
    @Test
    public void linearSearch_正常情况() {
        assertEquals(12, Searching.linearSearch(linearsearch,12));
        assertEquals(3,Searching.linearSearch(linearsearch,3));
        assertEquals(6,Searching.linearSearch(linearsearch,6));
        assertEquals(9,Searching.linearSearch(linearsearch,9));
//        assertEquals(true,Searching.linearSearch(linearsearch,0,linearsearch.length-1,6));
//        assertEquals(true,Searching.linearSearch(linearsearch,0,linearsearch.length-1,9));
//        assertEquals(true,Searching.linearSearch(linearsearch,0,linearsearch.length-1,3));
//        assertEquals(true,Searching.linearSearch(linearsearch,0,linearsearch.length-1,6));
//        assertEquals(true,Searching.linearSearch(linearsearch,0,linearsearch.length-1,9));
    }
    @Test
    public void linearSearch_异常情况(){
        assertEquals(null,Searching.linearSearch(linearsearch,200));
        assertEquals(null,Searching.linearSearch(linearsearch,1560));
        assertEquals(null,Searching.linearSearch(linearsearch,12321));
        assertEquals(null,Searching.linearSearch(linearsearch,237704));
//        assertEquals(false,Searching.linearSearch(linearsearch,8,7,200));
//        assertEquals(false,Searching.linearSearch(linearsearch,0,linearsearch.length-1,560));
//        assertEquals(false,Searching.linearSearch(linearsearch,0,linearsearch.length-1,770));
    }
    @Test
    public void linearSearch_边界情况(){
        assertEquals(1,Searching.linearSearch(linearsearch,1));
        assertEquals(4,Searching.linearSearch(linearsearch,4));
    }


    @Test
    public void BinarySearch_正常情况(){
        assertEquals(3,Searching.BinarySearch_non_recursion(binarysearch,3));
        assertEquals(6,Searching.BinarySearch_non_recursion(binarysearch,6));
        assertEquals(9,Searching.BinarySearch_non_recursion(binarysearch,9));
        assertEquals(12, Searching.BinarySearch_non_recursion(binarysearch,12));
    }
    @Test
    public void BinarySearch_异常情况(){
        assertEquals(null,Searching.BinarySearch_non_recursion(binarysearch,200));
        assertEquals(null,Searching.BinarySearch_non_recursion(binarysearch,1827));
        assertEquals(null,Searching.BinarySearch_non_recursion(binarysearch,23421));
        assertEquals(null, Searching.BinarySearch_non_recursion(binarysearch,221141));
    }
    @Test
    public void BinarySearch_边界情况(){
        assertEquals(1,Searching.BinarySearch_non_recursion(binarysearch,1));
        assertEquals(2319,Searching.BinarySearch_non_recursion(binarysearch,2319));
    }

}