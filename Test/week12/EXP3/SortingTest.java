package week12.EXP3;

import org.junit.Test;
import week12.EXP3.cn.edu.besti.cs1723.TCM2319.Sorting;

import static org.junit.Assert.*;

public class SortingTest {

    @Test
    public void selectionSort_正常情况() {
        int[] comparable0 = new int[]{1,3,6,9,12,11,2319,4};
        assertEquals(Sorting.selectionSort(comparable0).equals("1 3 4 6 9 11 12 2319 "),true);
        int comparable1[] = new int[]{1,12,11,3,6,9,2319,4};
        assertEquals(Sorting.selectionSort(comparable1).equals("1 3 4 6 9 11 12 2319 "),true);
    }
    @Test
    public void selectionSort_异常情况() {
        int comparable0[] = new int[]{1,3,6,9,12,11,2319,4};
        assertEquals(Sorting.selectionSort(comparable0).equals("1 3 4 6 9 11 12 2319 "),true);
        int comparable1[] = new int[]{1,12,11,3,6,9,2319,4,12,11,3,6,9,};
        assertEquals(Sorting.selectionSort(comparable1).equals("1 3 3 4 6 6 9 9 11 11 12 12 2319 "),true);
    }
    @Test
    public void selectionSort_边界情况() {
        int comparable0[] = new int[]{1,3,6,9,12,2981090,11,2319,4};
        assertEquals(Sorting.selectionSort(comparable0).equals("1 3 4 6 9 11 12 2319 2981090 "),true);
        int comparable1[] = new int[]{-179847841,1,12,11,3,6,9,2319,4};
        assertEquals(Sorting.selectionSort(comparable1).equals("-179847841 1 3 4 6 9 11 12 2319 "),true);
    }
    @Test
    public void selectionSort_正序情况() {
        int comparable0[] = new int[]{1,3,4,6,9,11,12,23,2319};
        assertEquals(Sorting.selectionSort(comparable0).equals("1 3 4 6 9 11 12 23 2319 "),true);
        int comparable1[] = new int[]{0,2,4,6,8,10};
        assertEquals(Sorting.selectionSort(comparable1).equals("0 2 4 6 8 10 "),true);
    }
    @Test
    public void selectionSort_逆序情况() {
        int comparable0[] = new int[]{2319,12,11,9,6,4,3,1};
        assertEquals(Sorting.selectionSort(comparable0).equals("1 3 4 6 9 11 12 2319 "),true);
        int comparable1[] = new int[]{10,8,6,4,2,0};
        assertEquals(Sorting.selectionSort(comparable1).equals("0 2 4 6 8 10 "),true);
    }
}